#!/bin/env python2
import os, sys, inspect, shutil
from distutils.dir_util import mkpath, copy_tree

IS_WIN = os.name == 'nt'
GIT = 'git'
HOME_DIR = os.path.expanduser('~')
NEOBUNDLE_PATH = os.path.join(HOME_DIR, '.vim', 'bundle', 'neobundle.vim')
CTAGS_PATH = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'bin', 'ctags')


def main():
    mkpath(NEOBUNDLE_PATH)
    has_neobundle = os.path.isdir(os.path.join(NEOBUNDLE_PATH, '.git'))
    if not has_neobundle:
        print 'Downloading NeoBundle using git'
        os.system('%s clone https://github.com/Shougo/neobundle.vim %s' % (GIT, NEOBUNDLE_PATH))
    else:
        print 'NeoBundle found; updating'
        os.chdir(NEOBUNDLE_PATH)
        os.system('%s pull' % GIT)
    print 'Writing config to ~/.vimrc'
    write_to_file(os.path.join(HOME_DIR, '.vimrc'), CONFIG)
    print 'Installing ctags'
    install_ctags()
    print 
    print "Note: Add 'export TERM=xterm-256color' to ~/.bashrc if necessary"
    print 'Press Ctrl-P in vim to view menu'


# TODO
def install_ctags():
    if IS_WIN:
        new_ctags_path = os.path.join(HOME_DIR, '.vim', 'bundle', 'ctags')
        copy_tree(CTAGS_PATH, new_ctags_path)
        append_to_path(new_ctags_path)


CONFIG = r"""set nocompatible
set runtimepath+=~/.vim/bundle/neobundle.vim/
call neobundle#begin(expand('~/.vim/bundle/'))
  " Bundle
  NeoBundleFetch 'Shougo/neobundle.vim'

  " Syntax
  NeoBundle 'sheerun/vim-polyglot'
  
  " IDE
  NeoBundle 'embear/vim-localvimrc'
  NeoBundle 'joonty/vdebug'

  " UI
  NeoBundle 'Shougo/unite.vim'
  NeoBundle 'thinca/vim-fontzoom'
  NeoBundle 'itchyny/lightline.vim'
  NeoBundle 'nathanaelkane/vim-indent-guides'
  NeoBundle 'ujihisa/unite-colorscheme'
  
  " AutoCompletetion
  NeoBundle 'shougo/neocomplcache.vim'

  " Themes: 
  " NeoBundle 'altercation/vim-colors-solarized'
  NeoBundle 'flazz/vim-colorschemes'
  " NeoBundle 'tomasr/molokai'
  " NeoBundle 'nanotech/jellybeans.vim'
  NeoBundle 'chriskempson/base16-vim'
  " NeoBundle 'sjl/badwolf'

  " Others:
  NeoBundle 'rosenfeld/conque-term'
  NeoBundle 'jakedouglas/exuberant-ctags'
  NeoBundle 'shougo/neomru.vim'
  NeoBundle 'majutsushi/tagbar'
  NeoBundle 'fidian/hexmode'
  NeoBundle 'tpope/vim-fugitive'
  NeoBundle 'scrooloose/nerdtree'
  NeoBundle 'scrooloose/syntastic'
  NeoBundle 'tpope/vim-surround'
  NeoBundle 'xolox/vim-misc'
  NeoBundle 'scrooloose/nerdcommenter'
  
  call neobundle#end()

NeoBundleCheck
set autochdir
set visualbell t_vb=
if has("autocmd") && has("gui")
    au GUIEnter * set t_vb=
endif
set laststatus=2
set encoding=utf8
filetype on
filetype plugin on
let g:neocomplcache_enable_at_startup = 1
set sessionoptions-=help
set background=light
colorscheme blue
set nobackup
set nowritebackup
set shortmess+=I
filetype plugin indent on
set tabstop=4
set shiftwidth=4
set smarttab
set expandtab
set cin
set ai
set showmatch
set hlsearch
set incsearch
set ignorecase
set nu
set guioptions-=T
set guioptions-=m 
set backspace=indent,eol,start
set guifont=Consolas:h16:cRUSSIAN
set keymodel=startsel,stopsel
set selection=exclusive
set selectmode=mouse,key
set whichwrap=b,s,<,>,[,]
syntax on



" let g:unite_candidate_icon = ">> "
" let g:unite_marked_icon = ">> "
let g:unite_split_rule = "botright"
let g:lightline = {
      \ 'active': {
      \   'left': [ [ 'filename' ],  [ 'readonly', 'modified' ], [ 'fugitive' ] ]
      \ },
      \ 'component': {
      \   'readonly': '%{&filetype=="help"?"":&readonly?"readonly":""}',
      \   'modified': '%{&filetype=="help"?"":&modified?"+":&modifiable?"":"-"}',
      \   'fugitive': '%{exists("*fugitive#head")?fugitive#head():""}'
      \ },
      \ 'component_visible_condition': {
      \   'readonly': '(&filetype!="help"&& &readonly)',
      \   'modified': '(&filetype!="help"&&(&modified||!&modifiable))',
      \   'fugitive': '(exists("*fugitive#head") && ""!=fugitive#head())'
      \ },
      \ }


" Initialize Unite's global list of menus
if !exists('g:unite_source_menu_menus')
    let g:unite_source_menu_menus = {}
endif

let menus = g:unite_source_menu_menus
" Create an entry for our new menu of commands
let menus.general = { 'description': 'My Commands' }
let menus.git = { 'description': 'Git commands' }
let menus.project = { 'description': 'Project commands' }

let g:localvimrc_ask = 0
let g:localvimrc_sandbox = 0
" let g:localvimrc_debug = 100
let g:localvimrc_persistent = 2


" Define the function that maps our command labels to the commands they execute
function! menus.general.map(key, value)
    return {
    \   'word': a:key,
    \   'kind': 'command',
    \   'action__command': a:value
    \ }
endfunction


function! menus.git.map(key, value)
    return {
    \   'word': a:key,
    \   'kind': 'command',
    \   'action__command': a:value
    \ }
endfunction


function! menus.project.map(key, value)
    return {
    \   'word': a:key,
    \   'kind': 'command',
    \   'action__command': a:value
    \ }
endfunction



function! NERDTreeToggleInCurDir()
  " If NERDTree is open in the current buffer
  if (exists("t:NERDTreeBufName") && bufwinnr(t:NERDTreeBufName) != -1)
    exe ":NERDTreeClose"
  else
    exe ":NERDTreeFind"
  endif
endfunction


" TODO
function! DumpEmptyLocalVimrc()
    exe ':new .lvimrc'
    if !filereadable(".lvimrc")
        call append(0, '" default lvimrc')
        call append(1, 'if g:localvimrc_sourced_once | finish | endif')
        call append(2, '')
        call append(3, 'let project_cmds = menus.project.command_candidates')
        call append(4, '')
        call append(5, 'color base16-greenscreen')
        call append(6, '')
        call append(7, 'call add(project_cmds, ["> Run in console [F5]", "ConqueTermVSplit node index.js"])')
        call append(8, 'call add(project_cmds, ["> Run Vdebug [F6]", "VdebugStart"])')
        call append(9, '')
        call append(10, 'nnoremap <F5> :ConqueTermVSplit node index.js<CR>')
        call append(11, 'nnoremap <F6> :VdebugStart<CR>')
        exe ':w!'
    endif
endfunction




" \   ['> Browse current dir', 'VimFilerBufferDir'],
" Define our list of [Label, Command] pairs
let menus.general.command_candidates = [
\   ['> Recent files [Ctrl-o]', 'Unite neomru/file'],
\   ['> Comment in/out [\c<space>]', 'call NERDComment(0, "toggle")'],
\   ['> Grep from current dir', 'UniteWithCurrentDir vimgrep'],
\   ['> Toggle NERDtree [Tab]', 'call NERDTreeToggleInCurDir()'],
\   ['> Pick colorscheme', 'Unite colorscheme'],
\   ['> View hex', 'Hexmode'],
\   ['> Command history', 'Unite history/unite'],
\   ['> Toggle tagbar', 'TagbarToggle'],
\   ['> Buffers', 'Unite buffer'],
\ ]


" TODO
let menus.git.command_candidates = [
\   ['> git add -A', 'Git add -A'],
\   ['> git commit', 'Gcommit'],
\   ['> git push', 'Git push'],
\   ['> git pull', 'Gpull'],
\   ['> git log', 'Git log'],
\   ['> git blame', 'Gblame'],
\   ['> git diff', 'Gdiff'],
\   ['> git status', 'Gstatus'],
\ ]


let menus.project.command_candidates = [
\   ['> Git commands', 'Unite menu:git'], 
\   ['> Change\Create localvimrc', 'call DumpEmptyLocalVimrc()'],
\ ]


autocmd VimEnter * echo "Press Ctrl-p, Ctrl-m to view menus"
autocmd VimEnter * IndentGuidesEnable
"autocmd VimEnter * TagbarToggle
"autocmd VimEnter * NERDTreeToggle
nnoremap <C-p> :Unite menu:general<CR>
nnoremap <C-o> :Unite neomru/file<CR> 
nnoremap <C-m> :Unite menu:project<CR> 
nnoremap <Tab> :call NERDTreeToggleInCurDir()<CR>
"""

if IS_WIN: CONFIG += "lan English"

def append_to_path(new_path):
    import _winreg as w
    rights = w.KEY_SET_VALUE | w.KEY_QUERY_VALUE 
    env_path = 'System\CurrentControlSet\Control\Session Manager\Environment'
    key = w.OpenKey(w.HKEY_LOCAL_MACHINE, env_path, 0, rights)
    path = w.QueryValueEx(key, 'PATH')[0]
    if not new_path in path:
        path += ';%s' % new_path
        w.SetValueEx(key, 'PATH', 0, w.REG_SZ, path)




def write_to_file(fname, text):
    with open(fname, 'w') as f:
        f.write(text)

if __name__=='__main__':
    main()
